/**
 *
 */
function editErrerCheck() {

	/* const ⇨　定数(再代入不可)
	　　var let  ⇨  変数(再代入可)　*/

	const name = document.getElementById('name').value;
	const nameLimit = document.getElementById('nameLimit')
	const nameEmpty = document.getElementById('nameEmpty')

	if (name == "") {
		nameEmpty.style.display = 'inline';
		return false;
	} else if (name.length >= 10) {
		nameLimit.style.display = 'inline';
		return false;
	}

	const login_id = document.getElementById('login_id').value;
	const login_idEmpty = document.getElementById('login_idEmpty')
	const login_idLimit = document.getElementById('login_idLimit')
	const login_idRegex = document.getElementById('login_idRegex')

	if (login_id == ""){
		login_idEmpty.style.display = 'inline';
		return false;
	} else if (login_id.length < 6 || 20 < login_id.length){
		login_idLimit.style.display = 'inline';
		return false;
	} else if (!login_id.match(/^[a-zA-Z0-9]{6,20}/)){
		login_idRegex.style.display = 'inline';
		return false;
	}

	const pw = document.getElementById('pw').value;
	const pwEmpty = document.getElementById('pwEmpty')
	const pwLimit = document.getElementById('pwLimit')
	const pwRegex = document.getElementById('pwRegex')

	if (pw == ""){
		pwEmpty.style.display = 'inline';
		return false;
	} else if (pw.length < 6 || 20 < pw.length){
		pwLimit.style.display = 'inline';
		return false;
	} else if (!pw.match(/^[a-zA-Z0-9]{6,20}/)){
		pwRegex.style.display = 'inline';
		return false;
	}
	const secondPw = document.getElementById('secondPw').value;
	const secondPwEmpty = document.getElementById('secondPwEmpty');
	const secondPwMismatch = document.getElementById('secondPwMismatch');

	if (secondPw == ""){
		secondPwEmpty.style.display = 'inline';
		return false;
	} else if (secondPw != pw){
		secondPwMismatch.style.display = 'inline';
		return false;

}


}