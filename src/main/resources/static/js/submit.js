/**
 *
 */

function submitErrerCheck() {

	/* const ⇨　定数(再代入不可)
	   var let  ⇨  変数(再代入可)　*/

	const subject = document.getElementById('subject').value;
	const subjectLimit = document.getElementById('subjectLimit')
	const subjectEmpty = document.getElementById('subjectEmpty')
	const message = document.getElementById('message').value;
	const messageLimit = document.getElementById('messageLimit')
	const messageEmpty = document.getElementById('messageEmpty')

	if (subject == "") {
		subjectEmpty.style.display = 'inline';
		return false;
	} else if (subject.length >= 30) {
		subjectLimit.style.display = 'inline';
		return false;
	}

	if (message == "") {
		messageEmpty.style.display = 'inline';
		return false;
	} else if (subject.length >= 1000) {
		messageLimit.style.display = 'inline';
		return false;
	}
}