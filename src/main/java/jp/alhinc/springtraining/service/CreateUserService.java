package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;
	//ログインIDの重複確認
	public int getLoginId(String login_id) {
		return mapper.getLoginId(login_id);
	}

	//新規登録機能（insert）
	@Transactional
	public int create(CreateUserForm form) {
		User entity = new User();
		entity.setLogin_id(form.getLogin_id());
		entity.setLogin_id(form.getLogin_id());
		entity.setName(form.getName());
		entity.setPassword(form.getRawPassword());
		entity.setDivision_id(form.getDivision_id());
		entity.setPosition_id(form.getPosition_id());
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));
		return mapper.create(entity);
	}

}
