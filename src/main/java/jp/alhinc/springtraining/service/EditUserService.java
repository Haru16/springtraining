package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class EditUserService {

	@Autowired
	private UserMapper mapper;

	//ログインIDの重複確認
	public int getLoginId(String login_id) {
		return mapper.getLoginId(login_id);
	}

	//編集機能（update）
	@Transactional
	public int edit(EditUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		entity.setLogin_id(form.getLogin_id());
		entity.setName(form.getName());
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));
		entity.setDivision_id(form.getDivision_id());
		entity.setPosition_id(form.getPosition_id());
		return mapper.edit(entity);
	}


}
