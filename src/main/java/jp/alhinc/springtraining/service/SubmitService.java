package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Submit;
import jp.alhinc.springtraining.form.SubmitForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class SubmitService {


	@Autowired
	private UserMapper mapper;

	public List<Submit> getSubmit() {
		return mapper.getSubmit();
	}

	@Transactional
	public int submit(SubmitForm form) {
		Submit entity = new Submit();
		entity.setSubject(form.getSubject());
		entity.setMessage(form.getMessage());
		return mapper.submit(entity);
	}



}
