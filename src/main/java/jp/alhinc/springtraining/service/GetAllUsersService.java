package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Division;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.Stop;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.form.StopUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class GetAllUsersService {

	@Autowired
	private UserMapper mapper;

	//ユーザ情報取得
	@Transactional
	public List<User> getAllUsers() {
		return mapper.findAll();
	}
	//停止・復元切替機能（update）
	public int getUpdateIsDelete(StopUserForm form){
		Stop entity = new Stop();
		entity.setId(form.getId());
		entity.setIs_delete(form.getIs_delete());
		return mapper.updateIsDelete(entity);
	}
	//支店取得
	@Transactional
	public List<Division> getDivision()  {
		return mapper.getDivision();
	}
	//部署・役職取得
	@Transactional
	public List<Position> getPosition()  {
		return mapper.getPosition();
	}
	//編集画面ユーザ情報取得
	public EditUserForm getEditUser(Long id){
		User editUser = mapper.getEditUser(id);
		EditUserForm form = new EditUserForm();
		BeanUtils.copyProperties(editUser, form);
		form.setRawPassword(editUser.getPassword());
		return form;
	}

}
