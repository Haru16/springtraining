package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.alhinc.springtraining.entity.Division;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.Stop;
import jp.alhinc.springtraining.entity.Submit;
import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	//ホーム画面ユーザ一覧用値取得
	List<User> findAll();

	//ログインID重複確認用値取得
	int getLoginId(@Param(value="login_id")String login_id);

	//新規登録
	int create(User entity);

	//ユーザ情報編集
	int edit(User entity);

	//id指定で編集画面表示用値取得
	User getEditUser(@Param(value="id")Long id);

	//プルダウン用値取得
	List<Division> getDivision();
	List<Position> getPosition();

	//ユーザ停止・復元ボタン変更処理
	int updateIsDelete(Stop entity);

	//投稿機能
	int submit(Submit entity);

	//投稿表示
	List<Submit> getSubmit();
}


