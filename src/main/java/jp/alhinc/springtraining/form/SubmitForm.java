package jp.alhinc.springtraining.form;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SubmitForm {

	@NotBlank(message="ログインIDを入力してください")
	@Size(max=30,message="30文字以内で入力して下さい")
	public String subject;

	@NotBlank(message="ログインIDを入力してください")
	@Size(max=1000,message="1000文字以内で入力して下さい")
	public String message;

	public Date date;


	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}



}
