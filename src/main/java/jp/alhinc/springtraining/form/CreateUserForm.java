package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateUserForm implements Serializable {

	@NotBlank(message="ログインIDを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message="半角英数字のみ有効です")
	public String login_id;

	@NotBlank(message="名前を入力してください")
	@Size(max=10,message="10文字以下で入力して下さい")
	public String name;

	@NotBlank(message="PWを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}",message="半角英数字のみ有効です")
	public String rawPassword;

	@NotBlank(message="確認用パスワードを入力してください")
	public String secondPassword;

	@AssertTrue(message="一致していません")
	public boolean isPasswordValid() {
		if(rawPassword.equals(secondPassword))
			return true;
		return false;
	}
	public int division_id;

	public int position_id;

	public String division_name;

	public String position_name;

	public int is_delete;



	public int getIs_delete() {
		return is_delete;
	}

	public void setIs_delete(int is_delete) {
		this.is_delete = is_delete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public int getDivision_id() {
		return division_id;
	}

	public void setDivision_id(int division_id) {
		this.division_id = division_id;
	}

	public int getPosition_id() {
		return position_id;
	}

	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}

	public String getDivision_name() {
		return division_name;
	}

	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}

	public String getPosition_name() {
		return position_name;
	}

	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}

	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}
}
