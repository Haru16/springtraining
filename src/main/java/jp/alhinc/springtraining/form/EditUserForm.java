package jp.alhinc.springtraining.form;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;



public class EditUserForm {

	private Long id;

	@NotEmpty(message = "名前を入力してください")
	@Size(max=10,message="10文字以下で入力して下さい")
	private String name;

	@NotBlank(message="ログインIDを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message="半角英数字のみ有効です")
	private String login_id;

	@NotBlank(message="PWを入力してください")
	@Size(min=6, max=20,message="6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}",message="半角英数字のみ有効です")
	private String rawPassword;

	@NotBlank(message="確認用PWを入力してください")
	public String secondPassword;

	@AssertTrue(message="一致していません")
	public boolean isPasswordValid() {
		if(rawPassword.equals(secondPassword))
		return true;
		return false;
	}

	private int division_id;

	private int position_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public int getDivision_id() {
		return division_id;
	}

	public void setDivision_id(int division_id) {
		this.division_id = division_id;
	}

	public int getPosition_id() {
		return position_id;
	}

	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}

	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}

}
