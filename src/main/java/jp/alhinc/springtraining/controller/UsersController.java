package jp.alhinc.springtraining.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Division;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.Submit;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.form.StopUserForm;
import jp.alhinc.springtraining.form.SubmitForm;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.SubmitService;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private EditUserService editUserService;

	@Autowired
	private SubmitService submitService;

	//定数化
	public static final String STR_POSITION = "position";
	public static final String STR_DIVISION = "division";
	public static final String STR_FORM = "form";
	public static final String STR_MESSAGE = "message";
	public static final String STR_USERS= "users";
	public static final String STR_SUBMIT="submit";


	//ユーザ一覧初期表示
	@GetMapping
	public String index(Model model) {
		List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute(STR_USERS, users);
		return "users/index";
	}
	//新規登録画面表示
	@GetMapping("/create")
	public String create(Model model) {
		//プルダウン用値取得
		List<Division> division = getAllUsersService.getDivision();
		List<Position> position = getAllUsersService.getPosition();
		model.addAttribute(STR_DIVISION,division);
		model.addAttribute(STR_POSITION,position);
		model.addAttribute(STR_FORM, new CreateUserForm());
		return "users/create";
	}
	//編集画面表示
	@GetMapping("/edit")
	public String edit(@RequestParam Long id,Model model) {
		//id指定で編集画面を表示させる
		EditUserForm editUsers = getAllUsersService.getEditUser(id);
		//プルダウン用値取得
		List<Division> division = getAllUsersService.getDivision();
		List<Position> position = getAllUsersService.getPosition();
		model.addAttribute(STR_DIVISION,division);
		model.addAttribute(STR_POSITION,position);
		model.addAttribute(STR_FORM, editUsers);
		return "users/edit";
	}

	//匿名掲示板機能
	@GetMapping("/submit")
	public String submit(Model model) {
		List<Submit> submit = submitService.getSubmit();

		model.addAttribute(STR_SUBMIT,submit);
		model.addAttribute("form", new Submit());
		return "users/submit";
		}

	//新規登録機能
	@PostMapping
	public String create(@ModelAttribute("form") @Validated CreateUserForm form, BindingResult result, Model model) {
		//プルダウン用値取得
		List<Division> division = getAllUsersService.getDivision();
		List<Position> position = getAllUsersService.getPosition();
		//バリデーションに引っ掛かったら
		if (result.hasErrors()) {
			model.addAttribute(STR_DIVISION,division);
			model.addAttribute(STR_POSITION,position);
			return "users/create";
		}

		int loginId = createUserService.getLoginId(form.getLogin_id());
		//バリデーションを抜けて重複しなかった場合
		if(loginId == 0) {
			createUserService.create(form);
			return "redirect:/users";

			//重複の場合
		} else {
			model.addAttribute(STR_DIVISION,division);
			model.addAttribute(STR_POSITION,position);
			model.addAttribute(STR_MESSAGE, "ログインIDが既に存在しています。");
			return "users/create";
		}
	}
	//停止・復元機能
	@PostMapping("/stop")
	public String stop(StopUserForm form, Model model) {
		getAllUsersService.getUpdateIsDelete(form);
		return "redirect:/users";
	}
	//編集機能
	@PostMapping("/edit")
	public String edit(@ModelAttribute("form") @Validated EditUserForm form, BindingResult result, Model model) {
		//プルダウン用値取得
		List<Division> division = getAllUsersService.getDivision();
		List<Position> position = getAllUsersService.getPosition();
		//バリデーションに引っ掛かった場合
		if (result.hasErrors()) {
			model.addAttribute(STR_DIVISION,division);
			model.addAttribute(STR_POSITION,position);
			return "users/edit";
		}
		//バリデーションに引っ掛からなかった場合
		int loginId = editUserService.getLoginId(form.getLogin_id());
		if(loginId == 0) {
			editUserService.edit(form);
			return "redirect:/users";
			//重複の場合
		} else {
			model.addAttribute(STR_DIVISION,division);
			model.addAttribute(STR_POSITION,position);
			model.addAttribute(STR_MESSAGE, "ログインIDが既に存在しています。");
			return "users/edit";
		}
	}

	//匿名掲示板機能
	@PostMapping("/submit")
		public String submit(@ModelAttribute("form") @Validated SubmitForm form,BindingResult result,Model model) {

		List<Submit> submit = submitService.getSubmit();
		model.addAttribute(STR_SUBMIT,submit);

		if (result.hasErrors()) {
			model.addAttribute(STR_SUBMIT,submit);
			return "users/submit";
		}

		submitService.submit(form);
			return "redirect:/users/submit";
	}
}

